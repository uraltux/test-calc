import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Converter {
    private int intNumber;
    private String romanNumber;
    private static TreeMap<Integer, String> map = new TreeMap<>();

    static {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(8, "VIII");
        map.put(7, "VII");
        map.put(6, "VI");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(3, "III");
        map.put(2, "II");
        map.put(1, "I");
    }


    public Integer getRomanToInt(String s) {
        for (Map.Entry<Integer, String> pair :
                map.entrySet()) {
            if (s.equals(pair.getValue())) {

                return pair.getKey();
            }
        }
        return null;

    }

    public String getIntToRoman(int a) {
        int l = map.floorKey(a);
        if ( a == l ) {
            return map.get(a);
        }
        return map.get(l) + getIntToRoman(a-l);
    }



}

