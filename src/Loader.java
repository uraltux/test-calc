import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

public class Loader extends Operations {
    public final static Converter converter = new Converter();
    public static int a;
    public static int b;
    public static int answer;
    public static String num0;
    public static String num1;
    public static Loader solver = new Loader();


    public static void main(String[] args) throws IOException {


        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter mathematical expression");
        while (true) {

            String Math = scanner.nextLine();
            bruteForce(Math);
        }

    }

    public static void bruteForce(String string) {
        String[] list = string.split(" ");
        //операции с арабскими числами
        if (checkIntNumber(list[0]) && checkIntNumber(list[2])) {
            a = Integer.parseInt(list[0]);
            b = Integer.parseInt(list[2]);
            if (0 < a && a <= 10 && 0 < b && b <= 10) {
                if (checkOperator(a, b, list[1])) {
                    System.out.println(answer);
                }
            }

        }
        //проверка на неправильный ввод
        if (!checkIntNumber(list[0]) && checkIntNumber(list[2]) || checkIntNumber(list[0]) && !checkIntNumber(list[2])) {
            throw new NumberFormatException("incorrect format");
        }
        //операции с римскими цифрами
        if (!checkIntNumber(list[0]) && !checkIntNumber(list[2])) {
            num0 = list[0];
            num1 = list[2];
            if (checkRomValue(num0) && checkRomValue(num1)) {
                a = converter.getRomanToInt(num0);
                b = converter.getRomanToInt(num1);
                if (checkOperator(a, b, list[1])) {
                    System.out.println(converter.getIntToRoman(answer));

                }
            }

        }
    }
 //проверка: число это или строка
    public static boolean checkIntNumber(String a) {

        try {
            Integer.parseInt(a);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
//проверка величины римской цифры
    public static boolean checkRomValue(String a) {
        if (0 < converter.getRomanToInt(a) && converter.getRomanToInt(a) <= 10) {

            return true;

        } else {
            throw new NumberFormatException("incorrect value");
        }

    }

    //проверка оператора
    public static boolean checkOperator(int a, int b, String s) {
        answer = solver.Decision(a, b, s);
        if (answer == -1) {
            throw new NumberFormatException("incorrect operator");

        } else {
            return true;
        }

    }


}
